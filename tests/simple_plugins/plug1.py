from stoepsel import Plugin

class MyPlugin(Plugin):
    name = 'simple_plugin'
    version = '0.0.1'
    dependencies = []

    def setup(self):
        self.register(self.PGM_MAIN,self.main)

    def main(self):
        print('Running around...')

